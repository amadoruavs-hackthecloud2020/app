//
//  ContentView.swift
//  MapView
//
//  Created by Derick Mathews on 7/11/20.
//  Copyright © 2020 Derick Mathews. All rights reserved.
//

import SwiftUI
import MapKit

struct ContentView: View {
    
    @State var manager = CLLocationManager()
    @State var alert = false
    @State var showLoginView: Bool = true
    @State private var centerCoordinate = CLLocationCoordinate2D()
    @State private var emergencyType = 0
    var emergencies = ["Police", "Ambulance", "Fire"]
    var images = ["🚓", "🚑", "🚒"]
    var body: some View {
        VStack
        {
            if showLoginView
            {
                HStack(alignment: .center)
                {
                    VStack(alignment: .center, spacing: 30.0)
                    {
                        Spacer()
                        
                        HStack
                        {
                                Text("Life Line")
                                    .font(.largeTitle)
                                    .fontWeight(.light)
                        }
                        Spacer()
                        Spacer()
                        Spacer()
                        Text("Sign In")
                            .fontWeight(.ultraLight)
                            .font(.title)
                        SingIn()
                        VStack
                        {
                            Spacer()
                            Spacer()
                            Spacer()
                            Button(action: {
                                print("Calling")
                                self.callNumber(phoneNumber: "+1911")
                                self.showLoginView = false
                            })
                            {
                                Text("Call Emergency Services")
                                .fontWeight(.semibold)
                                    .font(.headline)
                                Image(systemName: "phone.fill").resizable()
                                    .frame(width: 32.0, height: 32.0)
                                
                            }
                                .padding()
                                .foregroundColor(Color.white)
                            .background(Color.red)
                            .cornerRadius(20)
                            Spacer()
                            Picker(selection: $emergencyType, label: Text("What kind of emergency?")) {
                                ForEach(0..<3) { index in
                                    HStack
                                    {
                                        Text("\(self.emergencies[index])  \(self.images[index])").tag(index)
                                    }
                                }
                            }.pickerStyle(SegmentedPickerStyle())
                        }
                        Spacer()
                        
                    }
                }
            }
        else
        {
            ZStack
            {
                MapView(manager: $manager, alert: $alert, emergencyVehicle: images[emergencyType]).alert(isPresented: $alert) {
                    
                    Alert(title: Text("Please Enable Location Access In Settings Pannel !!!"))
                }
               .edgesIgnoringSafeArea(.all)
                        Button( action: {
                            self.showLoginView = true
                        })
                        {
                            Text("Exit")
                            .fontWeight(.light)
                                .font(.subheadline)
                            Image(systemName: "phone.down.fill")
                            
                            
                            
                        }
                            
                            .padding(10)
                            .foregroundColor(Color.white)
                        .background(Color.red)
                        .cornerRadius(20)
                        .offset(x: 0, y: 390)
            .opacity(50)
                    
                }

        }
    }
    }
    private func callNumber(phoneNumber:String) {
      if let phoneCallURL:NSURL = NSURL(string:"tel://\(phoneNumber)") {
        let application:UIApplication = UIApplication.shared
        if (application.canOpenURL(phoneCallURL as URL)) {
            application.open(phoneCallURL as URL);
        }
      }
    }

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct MapView : UIViewRepresentable {
    
    @Binding var manager : CLLocationManager
    @Binding var alert : Bool
    var emergencyVehicle : String
    let map = MKMapView()
    
    func makeCoordinator() -> MapView.Coordinator {
        return Coordinator(parent1: self, emergencyVehicle1: emergencyVehicle)
    }
    
    func makeUIView(context: UIViewRepresentableContext<MapView>) -> MKMapView {
        
        
        let center = CLLocationCoordinate2D(latitude: 13.086, longitude: 80.2707)
        let region = MKCoordinateRegion(center: center, latitudinalMeters: 1000, longitudinalMeters: 1000)
        map.region = region
        manager.requestWhenInUseAuthorization()
        manager.delegate = context.coordinator
        manager.startUpdatingLocation()
        return map
    }
    func updateUIView(_ uiView: MKMapView, context: UIViewRepresentableContext<MapView>) {
        
    }
    
    class Coordinator : NSObject,CLLocationManagerDelegate{
        
        var parent : MapView
        var emergencyVehicle : String
        
        init(parent1 : MapView, emergencyVehicle1 : String) {
            
            parent = parent1
            emergencyVehicle = emergencyVehicle1
        }
        
        func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            
            if status == .denied{
                
                parent.alert.toggle()
            }
        }
        
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            
            let location = locations.last
            let point = MKPointAnnotation()
            
            let georeader = CLGeocoder()
            georeader.reverseGeocodeLocation(location!) { (places, err) in
                
                if err != nil{
                    
                    print((err?.localizedDescription)!)
                    return
                }
                
//                let place = places?.first?.locality
                point.title = self.emergencyVehicle
                point.subtitle = "Current"
                point.coordinate = location!.coordinate
                self.parent.map.showsUserLocation = true
                self.parent.map.removeAnnotations(self.parent.map.annotations)
                ()
                self.parent.map.addAnnotation(point)
                let region = MKCoordinateRegion(center: location!.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)
                self.parent.map.region = region
                
            }
        }
    }
}

// if user denied location access then it must be activated through() settings but in simulator there is no app settings so I'm clearing location and privacy

// its updating live location .....
