//
//  Backend.swift
//  MapView
//
//  Created by Derick Mathews on 7/11/20.
//  Copyright © 2020 Derick Mathews. All rights reserved.
//
import Foundation
func postToEmergencyList(json: [String: Any])
{
    let jsonData = try? JSONSerialization.data(withJSONObject: json)
//    print(jsonData)
    let url = URL(string: "https://c1ad6fb86dd6.ngrok.io/api/v1/emergencies/")!
    var request = URLRequest(url: url)
//    print(request)
    request.httpMethod = "POST"
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpBody = jsonData
//    print(request.httpBody.)
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {
//            print(error?.localizedDescription ?? "No data")
            return
        }
        let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
        if let responseJSON = responseJSON as? [String: Any] {
//            print(responseJSON)
        }
        print(response)
    }
    task.resume()
}

func postToHealthInfo(json: [String: Any])
{
    let jsonData = try? JSONSerialization.data(withJSONObject: json)
    //    print(jsonData)
        let url = URL(string: "https://c1ad6fb86dd6.ngrok.io/api/v1/healthinfo/")!
        var request = URLRequest(url: url)
    //    print(request)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
    //    print(request.httpBody.)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
    //            print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
    //            print(responseJSON)
            }
            print(response)
        }
        task.resume()
}
